﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Vjesala
{
    public partial class Vjesala : Form
    {

        List<string> listaRijeci = new List<string>();
        string path = "C:\\Users\\Vlatka\\source\\repos\\Vjesala\\Vjesala\\Rijeci.txt";

        string RandomRijec;
        bool pobjeda = false;
        int pokusaji = 0;
        int broj = 0;
        int n;
        int pocetak = 0;
        public Vjesala()
        {
            InitializeComponent();
        }
        

        private void button1_Click(object sender, EventArgs e)
        {
            if (pobjeda || pocetak==0)
            {
                MessageBox.Show("\nDa biste pokrenuli igru\nkliknite tipku Start!");
            }
            else if (lbRijec.Text.ToUpper() == RandomRijec.ToUpper())
            {
                MessageBox.Show("Bravo!Pobijedili ste!");
                lbRijec.Text = string.Empty;
                lbPokusaji.Text = "10";
                pobjeda = true;
            }
            else if(pokusaji == 0)
            {
                string a = "Nazalost, izgubili ste... \nTrazena rijec je bila: ";
                a += RandomRijec;
                a += "\nDa biste ponovno pokrenuli igru\nkliknite tipku Start!";
                MessageBox.Show(a);
                lbRijec.Text = string.Empty;
                lbPokusaji.Text = "10";
            }
            else if(txtS.Text == string.Empty)
            {
                MessageBox.Show("Morate najprije unijeti slovo!");
            }
            else if(txtS.Text.Length > 1 )
            {
                MessageBox.Show("Dozvoljno je unosenje samo jednoga slova!");
            }
            else
            {
                pokusaji--;
           
                char a = txtS.Text[0];
                string rijec = lbRijec.Text;
                lbRijec.Text = string.Empty;
                for (int i = 0; i < n; i++)
                {

                    if (RandomRijec[i] == a || RandomRijec[i] == Char.ToUpper(a) || RandomRijec[i] == Char.ToLower(a))
                    {
                        lbRijec.Text += a;
                    }else if(rijec[i] != '*')
                    {
                        lbRijec.Text += rijec[i];
                    }
                    else
                    {
                        lbRijec.Text += '*';
                    }
                    
                }
                
                lbPokusaji.Text = Convert.ToString(pokusaji);
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            pobjeda = false;
            pocetak = 1;
            using (System.IO.StreamReader reader = new System.IO.StreamReader(@path))
            {
                string line;

                while ((line = reader.ReadLine()) != null) {
                    listaRijeci.Add(line);
                    broj++;
                }
            }
            //MessageBox.Show("Pokrenuli ste igru!");
            pokusaji = 10;
            Random r = new Random();
            int i = r.Next(0, broj);

            RandomRijec = listaRijeci[i];

            n = RandomRijec.Length;
            lbPokusaji.Text = "10";

            for (int j = 0; j < n; j++)
            {
                lbRijec.Text += '*';
               
            }
         
        }

        private void btnPogodiR_Click(object sender, EventArgs e)
        {
            if (pobjeda || pocetak == 0)
            {
                MessageBox.Show("Da biste pokrenuli igru\nkliknite tipku Start!");
            }
            else
            if(pokusaji == 0)
            {
                string a = "Nazalost, izgubili ste... \nTrazena rijec je bila: ";
                a += RandomRijec;
                a += "\nDa biste ponovno pokrenuli igru\nkliknite tipku Start!";
                MessageBox.Show(a);
                lbRijec.Text = string.Empty;
                lbPokusaji.Text = "10";
            }else
            if(txtR.Text == string.Empty)
            {
                MessageBox.Show("Morate najprije unijeti rijec!");
            }
            else if(txtR.Text.ToUpper() == RandomRijec.ToUpper())
                {
                    MessageBox.Show("Bravo! Pobijedili ste!");
                    lbRijec.Text = string.Empty;
                    lbPokusaji.Text = "10";
                    pobjeda = true;
                }
                else
                {
                    string a = "Nazalost, izgubili ste... \nTrazena rijec je bila: ";
                    a += RandomRijec;
                    a += "\nDa biste ponovno pokrenuli igru\nkliknite tipku Start!";
                    MessageBox.Show(a);
                    lbRijec.Text = string.Empty;
                    lbPokusaji.Text = "10";
                pokusaji = 0;
                }
            
        }
    }
}
