﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Znanstveni_Kalkulator
{
    public partial class Znanstveni : Form
    {
        string opr;
        double op1 = 0, op2 = 0, res = 0;
        public Znanstveni()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btn0_Click(object sender, EventArgs e)
        {
            lbUnos.Text += '0';
        }

        private void btnjednako_Click(object sender, EventArgs e)
        {
            if (lbUnos.Text == string.Empty)
            {
                MessageBox.Show("Najprije unesite broj!");
            }
            else
            {
                op2 = Convert.ToDouble(lbUnos.Text);
                lbUnos.Text = string.Empty;
                switch (opr)
                {
                    case "+":
                        res = op1 + op2;
                        lbRj.Text = Convert.ToString(res);
                        break;
                    case "-":
                        res = op1 - op2;
                        lbRj.Text = Convert.ToString(res);
                        break;
                    case "/":
                        if (op2 == 0)
                        {
                            MessageBox.Show("Nije dozvoljeno dijeliti s nulom!");
                            lbRj.Text = "0.0";
                            break;
                        }
                        else
                        {
                            res = op1 / op2;
                            lbRj.Text = Convert.ToString(res);
                            break;
                        }
                    case "*":
                        res = op1 * op2;
                        lbRj.Text = Convert.ToString(res);
                        break;
                    case "%":
                        res = op1 % op2;
                        lbRj.Text = Convert.ToString(res);
                        break;
                }
            }
        }

        private void btnplus_Click(object sender, EventArgs e)
        {
            if (lbUnos.Text == string.Empty)
            {
                MessageBox.Show("Najprije unesite broj!");
            }
            else
            {
                op1 = Convert.ToDouble(lbUnos.Text);
                opr = "+";
                lbUnos.Text = string.Empty;
            }
        }

        private void btnzarez_Click(object sender, EventArgs e)
        {
            if (lbUnos.Text.Contains("-"))
            {
                lbUnos.Text = lbUnos.Text;
            }
            else if(lbUnos.Text != string.Empty)
            {
                lbUnos.Text = lbUnos.Text;
            }
            else
            {
                lbUnos.Text = "-" + lbUnos.Text;
            }
            
           
        }

        private void btnnatrecu_Click(object sender, EventArgs e)
        {
            if(lbUnos.Text == string.Empty)
            {
                MessageBox.Show("Najprije unesite broj koji zelite potencirati!");
            }
            else
            {
                res = Math.Pow(Convert.ToDouble(lbUnos.Text), 3);
                lbRj.Text = Convert.ToString(res);
            }
           
        }

        private void btnminus_Click(object sender, EventArgs e)
        {
            if (lbUnos.Text == string.Empty)
            {
                MessageBox.Show("Najprije unesite broj!");
            }
            else
            {
                op1 = Convert.ToDouble(lbUnos.Text);
                opr = "-";
                lbUnos.Text = string.Empty;
            }
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            lbUnos.Text += '3';
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            lbUnos.Text += '2';
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            lbUnos.Text += '1';
        }

        private void btnnadrugu_Click(object sender, EventArgs e)
        {
            if (lbUnos.Text == string.Empty)
            {
                MessageBox.Show("Najprije unesite broj koji zelite potencirati!");
            }
            else
            {
                res = Math.Pow(Convert.ToDouble(lbUnos.Text), 2);
                lbRj.Text = Convert.ToString(res);
            }
        }

        private void btnputa_Click(object sender, EventArgs e)
        {
            if (lbUnos.Text == string.Empty)
            {
                MessageBox.Show("Najprije unesite broj!");
            }
            else
            {
                op1 = Convert.ToDouble(lbUnos.Text);
                opr = "*";
                lbUnos.Text = string.Empty;
            }
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            lbUnos.Text += '6';
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            lbUnos.Text += '5';
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            lbUnos.Text += '4';
        }

        private void btncos_Click(object sender, EventArgs e)
        {
            if (lbUnos.Text == string.Empty)
            {
                MessageBox.Show("Najprije unesite broj!");
            }
            else
            {
                res = Math.Cos(Convert.ToDouble(lbUnos.Text));
                lbRj.Text = Convert.ToString(res);
            }
            
        }

        private void btnkroz_Click(object sender, EventArgs e)
        {
            if (lbUnos.Text == string.Empty)
            {
                MessageBox.Show("Najprije unesite broj!");
            }
            else
            {
                op1 = Convert.ToDouble(lbUnos.Text);
                opr = "/";
                lbUnos.Text = string.Empty;
            }
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            lbUnos.Text += '9';
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            lbUnos.Text += '8';
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            lbUnos.Text += '7';
        }

        private void btnsin_Click(object sender, EventArgs e)
        {
            if (lbUnos.Text == string.Empty)
            {
                MessageBox.Show("Najprije unesite broj!");
            }
            else
            {
                if (btnpi.IsDisposed)
                {
                    res = 0;
                }
                else
                {
                    res = Math.Sin(Convert.ToDouble(lbUnos.Text));
                }
                lbRj.Text = Convert.ToString(res);
            }
            
        }

        private void btnmod_Click(object sender, EventArgs e)
        {
            if (lbUnos.Text == string.Empty)
            {
                MessageBox.Show("Najprije unesite broj!");
            }
            else
            {
                op1 = Convert.ToDouble(lbUnos.Text);
                opr = "%";
                lbUnos.Text = string.Empty;
            }
        }

        private void btnabs_Click(object sender, EventArgs e)
        {
            if (lbUnos.Text == string.Empty)
            {
                MessageBox.Show("Najprije unesite broj!");
            }
            else
            {
                res = Math.Abs(Convert.ToDouble(lbUnos.Text));
                lbRj.Text = Convert.ToString(res);
            }
        }

        private void btnkorijen_Click(object sender, EventArgs e)
        {
            if (lbUnos.Text == string.Empty)
            {
                MessageBox.Show("Najprije unesite broj!");
            }
            else
            {
                res = Math.Sqrt(Convert.ToDouble(lbUnos.Text));
                lbRj.Text = Convert.ToString(res);
            }
        }

        private void btnfak_Click(object sender, EventArgs e)
        {
            if (lbUnos.Text == string.Empty)
            {
                MessageBox.Show("Najprije unesite broj!");
            }
            else
            {
                int vr = 1;
                for (int i = 1; i <= Convert.ToDouble(lbUnos.Text); i++)
                {
                    vr *= i;
                }
                lbRj.Text = Convert.ToString(vr);
            }
        }

        private void btnln_Click(object sender, EventArgs e)
        {
            if (lbUnos.Text == string.Empty)
            {
                MessageBox.Show("Najprije unesite broj!");
            }
            else
            {
                res = Math.Log(Convert.ToDouble(lbUnos.Text));
                lbRj.Text = Convert.ToString(res);
            }
        }

        private void btnbackspace_Click(object sender, EventArgs e)
        {
            string str = lbUnos.Text;
            int n = str.Length;
            if(n == 0)
            {
                MessageBox.Show("Nema nista za obrisati");
            }
            else {
                lbUnos.Text = (str.Substring(0, n - 1));
            }

        }

        private void btnC_Click(object sender, EventArgs e)
        {
            lbUnos.Text = string.Empty;
        }

        private void btne_Click(object sender, EventArgs e)
        {
            if (lbUnos.Text != string.Empty)
            {
                MessageBox.Show("Vec ste unijeli broj!");
            }
            else
            {
                double en = (double)Math.E;
                lbUnos.Text = Convert.ToString(en);
            }
        }

        private void btnIzRj_Click(object sender, EventArgs e)
        {
            lbRj.Text = "Rjesenje";
        }

        private void btnpi_Click(object sender, EventArgs e)
        {
            if (lbUnos.Text != string.Empty)
            {
                MessageBox.Show("Vec ste unijeli broj!");
            }
            else
            {
                double pii = Math.PI;
                lbUnos.Text = Convert.ToString(pii);
            }
        }

        private void btnlog_Click(object sender, EventArgs e)
        {
            if (lbUnos.Text == string.Empty)
            {
                MessageBox.Show("Najprije unesite broj!");
            }
            else
            {
                res = Math.Log10(Convert.ToDouble(lbUnos.Text));
                lbRj.Text = Convert.ToString(res);
            }
        }

    }
}
